# windows-srm-deadman-test

This project serves as a simple [dead man's switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch) by running a simple job on the Windows Shared Runners every 2 hours (via scheduled pipeline). If it fails, it should alert us in #f_win_shared_runners in Slack.
